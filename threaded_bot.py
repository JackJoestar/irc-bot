#!/usr/bin/env python2

from threading import Thread
from Queue import Queue
from random import choice
from time import sleep
import socket
import ssl
import select

class Bot:
    
    def __init__( self, server, port, enable_ssl, nick, chan, threads ):
        
        #Bot configs arranged in a dictionary.
        self.config = {
            "server"     : server,
            "port"       : int( port ),
            "enable_ssl" : enable_ssl,
            "nick"       : nick,
            "chan"       : chan,
            "threads"    : int( threads )
        }
        
        #Bot status.
        self.state = {
            "sock"      : socket.socket( socket.AF_INET, socket.SOCK_STREAM ),
            "connected" : False,
            "inchan"    : False,
            "sendq"     : Queue(),
            "procq"     : Queue()
        }
        
        #Bot threads. Checks the number of threads in the configs, and creates them accordingly.
        self.threads = []
        for x in xrange( self.config["threads"] ):
            proc = Thread( target=self.processor )
            proc.daemon = True
            proc.start()
            self.threads.append( proc )
            print "started thread"
        
        #Bot opens a socket and connects to the irc server. If SSL is enabled, then the socket is wrapped with SSL.
        self.state["sock"].connect(( self.config["server"], self.config["port"] ))
        if self.config["enable_ssl"] is True:
            self.state["sock"] = ssl.wrap_socket( self.state["sock"] )
        
        self.state["sock"].send( "USER {} {} {} :hi\n".format( 
            self.config["nick"], self.config["nick"], self.config["nick"]
        ))
        self.state["sock"].send( "NICK {}\n".format( self.config["nick"] ) )
        self.state["sock"].setblocking( 0 )
        self.state["connected"] = True
    
    #Bot starts to run.
    def run( self ):
        buff = b""

        while True:
            
            lines = []
            
            if self.state["sendq"].empty() is not True and self.state["inchan"] is True:
                msg = self.state["sendq"].get()
                self.state["sock"].send( "PRIVMSG {} :{}\n".format( self.config["chan"], msg ) )
            else:
                ready = select.select( [self.state["sock"]], [], [], 1 )
                if ready[0]:
                    try:
                        raw = self.state["sock"].recv( 512 )
                        buff = buff + raw
                        lines = buff.split( "\n" )
                        buff = lines.pop()
                    except:
                        pass
    
                if len( lines ) > 0:
                    for line in lines:
                        print line
    
                        if line.find( "PING :" ) != -1:
                            self.state["sock"].send( "PONG :{}\n".format( line.split( ":" )[1] ) )
    
                        if "376" in line and self.state["inchan"] is False:
                            self.state["sock"].send( "JOIN {}\n".format( self.config["chan"] ) )
                            self.state["inchan"] = True
    
                        if "!prime" in line:
                            #this is our bogus function that just generates a random number
                            #so that our processor thread can do the heavy lifting
                            try:
                                n = line.split( "!prime " )[1]
                                self.state["procq"].put( n )
                                print "calculating all the primes between 0 and {}".format( str( n ) )
                            except:
                                pass


    def processor( self ):
        while True:
            if self.state["procq"].empty() is not True:
                #generate a bigger number from the random one we got
                n = int( self.state["procq"].get() )
                #then snag all the prime numbers in the range from 0 - rn using the Sieve of Eratosthenes
                ignore = []
                result = []
                for i in range( 2, n + 1 ):
                    for j in range( 2, n + 1 / i ):
                        if i * j <= n:
                            ignore.append( ( i * j ) )
                for i in range( 2, n + 1 ):
                    if i not in ignore:
                        result.append( i )
                #then send our answer to the sendq
                nlist = ",".join( [ str( x ) for x in result ] )
                answer = "Primes from 0 to {}: {}".format( str( n ), nlist )
                self.state["sendq"].put( answer )
                print "sent to sendq: {}".format( answer )
                self.state["procq"].task_done()
            else:
                sleep( 1 )
                        
if __name__ == "__main__":
    server = "irc.freenode.net"
    port = 6667
    enable_ssl = True
    nick = "Eratosthenes"
    chan = "##testingarea" 
    threads = 3
    bot = Bot( server, port, enable_ssl, nick, chan, threads )
    bot.run()
